const fs = require('fs')

function readFile(path, options = {}) {
    return new Promise((resolve, reject) => {
        fs.readFile(path, options, (err, data) => {
            if (err) return reject(err);
            resolve(data)
        })
    })
}

/* Promesses classiques */
// const promise = readFile('hello.txt', { encoding: 'utf-8' })
// promise
//     .then(data => {
//         console.log(data)
//     })
//     .catch(err => {
//         console.error(err)
//     })
//     .finally(() => {
//         console.log('finish')
//     })


/* Fonctions asynchrones */
async function test() {
    let data;
    try {
        data = await readFile('hello.txt', { encoding: 'utf-8' })
        console.log(data)
    }
    catch(err) {
        console.error(err)
    }
    finally {
        console.log('finally')
    }
    console.log('Bonjour !', data)
}

test()
