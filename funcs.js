function f1(a, b) {
    return a + b;
}

/**
 * 
 * @param {Number} a 
 * @param {Number} b 
 * @returns 
 */
const f2 = function f2(a, b) {
    return a - b;
}

/**
 * Fonction anonyme f3
 * @param {Number} a 
 * @param {Number} b 
 * @returns 
 */
const f3 = function(a, b) {
    return a * b;
}

const f4 = (c, d) => {
    return c + d
}

const f5 = (c, d) => c + d;

const r5 = f5(1, 2)

console.log(f1, f2, f3, f5, r5);