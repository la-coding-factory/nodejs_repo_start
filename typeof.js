var a;
console.log(typeof a);

a = "hello"
console.log(typeof a);

a = 42
console.log(typeof a);

a = true
console.log(typeof a);

a = null // object
console.log(typeof a);

a = undefined
console.log(typeof a);

a = { b: "c" } // object
console.log(typeof a);

a = ["42", "+", 12] // object
console.log(typeof a);
