
const c = 1
let coucou = 'Hello world'
let hello = "Hello World"

let isTrue = true 
let isFalse = false 

const HELLO = hello.toUpperCase()

console.log(a, c, coucou, hello, HELLO, isTrue, isFalse)

/*
document
alert
prompt
*/

console.log(process.argv)

function f1(a, b) {
    return a + b;
}

/**
 * 
 * @param {Number} a 
 * @param {Number} b 
 * @returns 
 */
const f2 = function f2(a, b) {
    return a - b;
}

/**
 * Fonction anonyme f3
 * @param {Number} a 
 * @param {Number} b 
 * @returns 
 */
const f3 = function(a, b) {
    return a * b;
}


// String & Numbers

var a = "12"
var an = Number(a)

console.log(a, an)

// Triple = conditions
if (a == an) {
    console.log('a == an')
}
if (a === an) {
    console.log('a === an')
}




for (let i = 0; i < 10; i++) {
    console.log('i', i);
}



// const obj = new Object()
// console.log({ hello: 'world' }, obj)